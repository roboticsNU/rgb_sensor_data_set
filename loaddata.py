from numpy import genfromtxt
import io
import os
import imageio
from os import listdir
from os.path import isfile, join
import numpy as np
import math
import PIL.Image as Image
import cv2
import re
import sys
import random
import pickle

def loaddata(path, loadfromdisk):
  if (loadfromdisk == 0):
    with open('/mnt/data/data_zhanat/trainvalid.pkl', 'r') as f:
      #pickle.dump([train_data, valid_data], f)
      mydbtrainimg, mydbtestimg, mydatatrain, mydatatest=pickle.load(f)
    return mydbtrainimg, mydbtestimg, mydatatrain, mydatatest

  imgpath = '/mnt/data/data_zhanat/images/'
  mydatatrain = genfromtxt(path + str('training.csv'), delimiter=',')
  mydatatest = genfromtxt(path + str('validation.csv'), delimiter=',')
  trainlen = mydatatrain.shape[0]
  testlen  = mydatatest.shape[0]

  firstcoltrain = mydatatrain[:,0]
  mydatatrain = mydatatrain[:, 1:mydatatrain.shape[1]]

  firstcoltest = mydatatest[:,0]
  mydatatest  = mydatatest[:, 1:mydatatest.shape[1]]

  mydbtrainimg = []
  mydbtestimg = [];

  print("output data shape")
  print(mydatatrain.shape)
  print(mydatatest.shape)
 
  for flindex in np.nditer(firstcoltrain):
    indtr = int(flindex)
    filename = 'imagexx' + str(indtr) + '.png'
    im = imageio.imread(os.path.join(imgpath, filename))
    im = np.array(im)
    img = Image.fromarray(im, 'RGB')
    img = img.convert("L")
    img = np.array(img)

    img =   img[175:465, 110 : 375]

    mydbtrainimg.append(img) 
 
  for flindex in np.nditer(firstcoltest):
    indtr = int(flindex)
    filename = 'imagexx' + str(indtr) + '.png'
    im = imageio.imread(os.path.join(imgpath, filename))
    im = np.array(im)
    img = Image.fromarray(im, 'RGB')
    img = img.convert("L")
    img = np.array(img)

    img =   img[175:465, 110 : 375]

    mydbtestimg.append(img)

  mydbtrainimg = np.array(mydbtrainimg)
  mydbtestimg = np.array(mydbtestimg)
  print("shape of training images db:")
  print(mydbtrainimg.shape)

  print("shape of testing images db:")
  print(mydbtestimg.shape)

  with open('/mnt/data/data_zhanat/trainvalid.pkl', 'w') as f:
    pickle.dump([mydbtrainimg, mydbtestimg, mydatatrain, mydatatest], f)

  return mydbtrainimg, mydbtestimg, mydatatrain, mydatatest

