function outputs = generateCnnData(dataset1)
    flag = 0;

    pairscount = 0;
    starti = [];
    endi = [];
    s1 = 0;
    e1 = 0;

    for i=1:size(dataset1, 1)
        val = dataset1(i, 8);
        if (val >= 2000 && val < 3000)
            if (flag == 0)
                s1 = i;
                flag = 1;
            end
        elseif (flag == 1)
            flag = 0;
            e1 = i - 1;
            pairscount = pairscount + 1;
            starti(pairscount) = s1;
            endi(pairscount) = e1;        
        end
    end

    outputs = [];
    for i=1:pairscount
        for j=starti(i):endi(i)
            id = dataset1(j, 1);
            fz = dataset1(j, 4);
            state = dataset1(j, 8);
            pointid = mod(state, 100);
            forceid = mod(state, 1000);
            forceid = floor(forceid / 100);
            label = 0;
            mypoint = getPointById(pointid);
            outputs = [outputs;[id, forceid, pointid, label]];
        end
    end
    
    minforce = min(outputs(:, 2));
    maxforce = max(outputs(:, 2));
    minpoint = min(outputs(:, 3));
    maxpoint = max(outputs(:, 3));
    forcecount = maxforce - minforce + 1;
    poitncount = maxpoint - minpoint + 1;
    
    for i=1:size(outputs, 1)
        forceid = outputs(i, 2) + 1;
        pointid = outputs(i, 3);
        value = (forceid - 1) * poitncount + pointid;
        outputs(i, end) = value - 1;
    end
end 

function point1 = getPointById(id)
%x1 x2 x3 x4
%1  2  3  4  y1
%8  7  6  5  y2
%9 10 11 12  y3
%16 15 14 13 y4
    if (id <= 4)
        point1 = [1,id];
    elseif (id <= 8)
        point1 = [2, 4-(id-4)+1];
    elseif (id <= 12)
        point1 = [3, id-8];
    elseif (id <= 16)
        point1 = [4, 4-(id-12)+1];
    end
end