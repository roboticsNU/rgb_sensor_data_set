result = generateCnnData(dataset1);
shuffledData = result(randperm(size(result,1)),:);

len = size(shuffledData, 1);
pos = floor(len * 0.8);
training = shuffledData(1:pos, :);
validation = shuffledData(pos + 1:end, :);

training = training(1:100, :);
validation = validation(1:100, :);
%%
data = csvread("Z:\Zhanat(muzhik)\rgb_sensor\exp3\train_labels_edited.csv");
data(:, [1,3,4]) = [];

shuffledData = data(randperm(size(data,1)),:);

len = size(shuffledData, 1);
pos = floor(len * 0.8);
training = shuffledData(1:pos, :);
validation = shuffledData(pos + 1:end, :);

%%
yfit=quadrocv5.predictFcn(validation(:, 2:end));
sum(yfit == validation(:, 1)) / size(validation, 1)

%%
yfit=quadrocv5pca.predictFcn(validation(:, 2:end));
sum(yfit == validation(:, 1)) / size(validation, 1)

%%
yfit=quadrocv5.predictFcn(training(:, 2:end));
sum(yfit == training(:, 1)) / size(training, 1)

%%
yfit=quadrocv5pca.predictFcn(training(:, 2:end));
sum(yfit == training(:, 1)) / size(training, 1)