headers:
Number of message, fx, fy,fz, fwx, fwy, fwz, action states, image feature[first 9 hsv average, 27 rgb average]

x0, y0 = []

deltax = 0.007 
deltay = 0.008
deltaz = 0.001

- action states - [2 1 1 6], here

1st number: 1 - down 
	    2 - pressed and hold
	    3 - up

2nd number: 0 - 1 mm depth
	    1 - 2 mm depth
	    ...
	    9 - 10 mm depth

3-4 number : point coordinates 
	    01 - 1st point []
	    02 - 2nd point 
	    ...
	
action states .. last two numbers
x1 x2 x3 x4
1  2  3  4  y1
8  7  6  5  y2
9 10 11 12  y3
16 15 14 13 y4
