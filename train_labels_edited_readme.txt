header:
image_id, unique_id, depth, pose_xy, feature_vectors

here:
image_id - ros message id, same as image id in the /images folder
unique_id - from 1 to 128, calculated as depth * 16 + pose_xy 
depth - from 0 to 7 , here 0 corresponds to 1 mm pressing, 1 - 2 mm pressing and so on..
pose_xy - from 1 to 16 locations where robot is pressing
feature_vectors - 27 vector array of RGB
