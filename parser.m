clear all; close all; clc;
fid = fopen('train_labels.csv', 'r');

dataset1 = [];
dataset2 = [];
counter = 0;
while (1 == 1)
    line = fgets(fid);
    if (line == -1)
        break;
    end
    splittedline = strsplit(line, ',');
    first8 = splittedline(1:8);
    arrayofvalues = join(splittedline(9:end));
    arrayofvalues = arrayofvalues{1};
    arrayofvalues = arrayofvalues(strfind(arrayofvalues, '['):strfind(arrayofvalues, ']'));
    arrayofvalues = arrayofvalues(2:end-1);
    arrayofvalues = str2num(arrayofvalues);
    
    v1 = first8{1};
    v2 = first8{2};
    v3 = first8{3};
    v4 = first8{4};
    v5 = first8{5};
    v6 = first8{6};
    v7 = first8{7};
    v8 = first8{8};
    
    v1 = str2num(v1);
    v2 = str2num(v2);
    v3 = str2num(v3);
    v4 = str2num(v4);
    v5 = str2num(v5);
    v6 = str2num(v6);
    v7 = str2num(v7);
    v8 = str2num(v8);
    
    counter = counter + 1;
    dataset1 = [dataset1;[v1, v2,v3,v4,v5,v6,v7, v8]];
    dataset2 = [dataset2;arrayofvalues];
    
    if (mod(counter, 100) == 0)
        clc;
        disp(int2str(100.0 * counter/56000.0) + "% done");
        drawnow;
    end
end
